// Original Author: Sean Davis
// Edits: Henry Walton, Lucian Novosel
#include <cstdlib>
#include <cstring>
#include "directory.h"
#include <iostream>
using namespace std;

Directory* Directory::cd(int argCount, const char *arguments[])
{
  if(argCount != 2)
  {
    cout << "usage: cd directory_Name" << endl;
    return this; //currentDirectory;
  } // if wrong number of arguments

  if(strcmp(arguments[1], "..") == 0)
  {
    if(parent)
      return parent;
    else
      return this;
  } // if cd ..

  for(int i = 0; i < subDirectoryCount; i++)
    if(strcmp(arguments[1], subDirectories[i]->name) == 0)
      return subDirectories[i];

  cout << arguments[1]<< " no such directory" << endl;
  return this;

} // cd()


Directory::Directory(const char *argname, short umask, int argtime, Directory *argparent)
{
  name = new char[strlen(argname)];
  strcpy(name, argname);
  permissions.set(7,umask);// P
  time = argtime;
  parent = argparent;
  subDirectoryCount = 0;
} // createDirectory()


void Directory::ls(int argCount, const char *arguments[]) const
{
  if(argCount > 2 || (argCount == 2 && strcmp(arguments[1], "-l")))
  {
    cout << "usage: ls [-l]"<< endl;
    return;
  }  // if 

  if(subDirectoryCount == 0)
    return;
    
  if(argCount == 2) // must be ls -l
  {
   for(int i = 0; i < subDirectoryCount; i++)
   {
      subDirectories[i]->permissions.print();
      cout << " " << subDirectories[i]->time << " " << subDirectories[i]->name << endl;
    } // for each subdirectory
  } // if -l
  else // simple ls
  {
    for(int i = 0; i < subDirectoryCount; i++)
      cout << subDirectories[i]->name << endl;
  } // else simple else
} // ls()


void Directory::mkdir(int argCount, const char *arguments[],
  short umask, int time)
{
  if(argCount != 2)
  {
    cout << "usage: mkdir directory_name" << endl;
    return;
  } // if wrong number of arguments

  if(subDirectoryCount == MAX_DIRECTORIES)
  {
    cout << "mkdir: "<< name <<" already contains the maximum number of directories" << endl;
    return;
  } // if already at max subdirectories

  for(int i = 0; i < subDirectoryCount; i++)
    if(strcmp(arguments[1], subDirectories[i]->name) == 0)
    {
      cout << "mkdir: cannot create directory " << arguments[1] << " File exists" << endl;
      return;
    } // for each sub directory

  subDirectories[subDirectoryCount++] = new Directory(arguments[1], umask, time, this);
                //(Directory*) malloc(sizeof(Directory));
                //Directory(arguments[1], umask, time, this);
} // mkdir()


void Directory::showPath(Directory *directory) const
{
    if (directory->parent)
  //if(directory->time != 0)
  {   
    showPath(directory->parent);
    cout << directory->name << "/";
        
  } // if has a parent
  else // directory has no parent, i.e. the root
    cout << directory->name;
} // showPath()


Directory::~Directory()
{
  for(int i=0;i<subDirectoryCount;i++)
  {
    delete this->subDirectories[i];
  }
}
