/* 
 * File:   Funix.h
 * Original Author: Sean Davis 
 * Edits: Henry Walton, Lucian Novosel
 *
 * Created on April 30, 2012, 10:35 PM
 */

#ifndef FUNIX_H
#define	FUNIX_H

#include "directory.h"

class Funix
{
private:
  Directory *currentDirectory;
  int umask;
  int time;
  void setUmask(int argCount, const char *arguments[]);
  // checks "umask" command and executes it if it is proper
  void writePrompt(); // shows path and '#'
  void cd(int argCount, const char *arguments[]);
  // calls cd() with currentDirectory as one of its parameters
  int eXit( int argCount, const char *arguments[]);
  // checks "exit" command, returns 0 on proper exit
  void getCommand( char *command); // writes prompt and reads command
  //void init(Funix *funix); // creates currentDirectory, and sets umask and time
  void ls( int argCount, const char *arguments[]);
  // calls ls() with currentDirectory as one of its parameters
  void mkdir( int argCount, const char *arguments[]);
  // calls mkdir() with currentDirectory as one of its parameters
  int processCommand( char *command); // returns 0 on proper exit

public:
Funix();//constructor 
void run();//Funix *funix); // reads and processes commands until proper exit
~Funix();
} ;
#endif
