// Original Author: Sean Davis
// Edits: Henry Walton, Lucian Novosel

#include <iostream>
#include "permissions.h"
using namespace std;

void Permissions::print() const
{
  if(octal & 4)
    cout <<"r";
  else // 4-bit not set
    cout <<"-";

  if(octal & 2)
    cout <<"w";
  else // 2-bit not set
    cout <<"-";

  if(octal & 1)
    cout <<"x";
  else // 1-bit not set
    cout <<"-";
} // print()


void Permissions::set(short originalPermissions, short umask)
{
  octal = originalPermissions & ~umask;
} // setPermissions()
