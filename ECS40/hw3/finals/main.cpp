// Original Author: Sean Davis
// Edits: Henry Walton, Lucian Novosel

#include <cstdlib>
#include "funix.h"

int main()
{
//	Funix *funix = (Funix*) malloc(sizeof(Funix));
    Funix* funix = new Funix();
  funix->run();
  delete funix;
} // main()
