// Original Author: Sean Davis
// Edits: Henry Walton, Lucian Novosel 
#ifndef DIRECTORY_H
#define	DIRECTORY_H

#define MAX_DIRECTORIES 3
#include "permissions.h"

class Directory
{

private:
  char *name;
  Directory* subDirectories[MAX_DIRECTORIES];
  int time;
  int subDirectoryCount;
  Permissions permissions;
  Directory* parent;

public:
  Directory(const char *name, short umask, int time, Directory *parent);
  Directory* cd(int argCount, const char *arguments[]);
  void ls(int argCount, const char *arguments[]) const;
  void mkdir(int argCount, const char *arguments[], short umask, int time);
  void showPath(Directory *directory) const;
  ~Directory();
};



#endif	/* DIRECTORY_H */

