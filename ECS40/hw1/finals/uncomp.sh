#! /bin/bash
#Henry Walton, Lucian Novosel

#decompresses depending on which compression type it uses

if [ $# -eq 0 ] ; then
	echo 'usage: uncomp {filelist}+'
	exit 
fi

case $1 in
	*.tar) tar -xvf $1 ;;
	*.tar.gz) tar -xvzf $1  ;;
	*.tar.Z) tar -xvZf $1 ;;
	*.Z) tar -xvZf $1 ;;
	*.gz) gunzip $1 ;;
	*.zip) unzip $1 ;;
	*) echo "$1 has no compression extension" ;;
esac

exit 1
