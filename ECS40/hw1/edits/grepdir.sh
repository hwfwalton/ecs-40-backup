#! /bin/bash
#Henry Walton, Lucian Novoscel

#checks if the first parameter is a valid directory
if [ !  -d $1 ]; then
	echo "usage: grepdir.sh directory pattern [-grep option]*"
	exit 1
fi

#checks if a second parameter was entered
if [ -z "$2" ]; then
	echo "usage: grepdir.sh directory pattern [-grep option]*"
	exit 1
fi

#uses find to search through the specified directory and subdirectories and 
#executes grep on each directory with the specified pattern
find $1 -exec grep "$2" $3 $4 $5 $6 $7 {} \;

exit 1
