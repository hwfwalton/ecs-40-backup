#! bin/bash/
#Henry Walton, Lucian Novosel

#checks that the user included an executable name
if [ -z "$1" ]; then
	echo "Executable name required."
	echo "usage: make.sh executable_name"
	exit 1
fi

#defines the extra parameters as blank unless the user entered specific
#additional ones
if [ ! -z "$2" ]; then
	PARA1=" $2"
fi

if [ ! -z "$3" ]; then
	PARA2=" $3"
fi

if [ ! -z "$4" ]; then
	PARA3=" $4"	
fi

if [ ! -z "$5" ]; then
	PARA4=" $5"
fi
	
#makes a list of all the object files
OBJECTS=$(find . -name "*.cpp" -printf "%P " | sed s/\.cpp/\.o/g)

#write the first two lines of the make file
echo -e "$1 : $OBJECTS\n\tg++ -ansi -Wall -g -o$PARA1$PARA2$PARA3$PARA4 $1 $OBJECTS" > Makefile

find . -name "*.cpp" | tr ' ' '\n'>files.txt

#loops through the filenames stored in file.txt and executes the 'do' code on
#each file name in sequence, and then appends the new lines to the Makefile
exec 3<&0
exec 0<files.txt
  while read line 
    do
	DEPENDENCIES=$(grep -i "#include \"" $line | sed s:"\""::g  | sed s/"#include "//i | tr '\n' ' ')
	OBJECT=$(echo $line | sed s:./:: | sed s:.cpp::)
	echo -e "$OBJECT.o : $DEPENDENCIES\n\tg++ -ansi -Wall -g -c$PARA1$PARA2$PARA3$PARA4 $OBJECT.cpp" >> Makefile
    done
exec 0<&3

#write the clean statement
echo -e "clean : \n\trm -f $1 $OBJECTS" >> Makefile

#deletes the list file we used to store the file names
rm -f files.txt

exit 1;
