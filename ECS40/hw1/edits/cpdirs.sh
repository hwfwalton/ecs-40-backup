#! /bin/bash
#Henry Walton, Lucian Novosel

#checks if the first two directories exist, and that the third directory does 
#not exist
if [ ! -d $1 ]; then
        echo "usage: cpdirs.sh source_directory1 source_directory2 dest_directory"
        exit 1
elif [ ! -d $2 ]; then
	echo "usage: cpdirs.sh source_directory1 source_directory2 dest_directory"
	exit 1
elif [ ! -d $3 ]; then
	mkdir $3
fi

#creates the folder that I'm going to backup the second directory into
mkdir backup

#copies the contents of the second directory into backup so we can restore it
#to its original state once we've copied the first directory files into
cp ./$2/* ./backup

#Copies the files of the first directory into the second, keeping the newer
#versions of any files that are found in both and then copies all the files
#to the third directory
cp -u ./$1/* $2 ; cp -u ./$2/* $3

#restores the second directory to its original state using the files in backiup
rm -f ./$2/* ; cp ./backup/* $2 ; rm -f ./backup/* ; rmdir backup

exit 1
