#! bin/bash/
#Henry Walton, Lucian Novosel

#DEPENDENCIES=$(grep -i "#include" $1 | sed s/"#include "//i | tr '\n' ' ')

#OBJECT=$(echo $1 | sed s/.\/// | sed s/\.cpp//g)

#echo -e "$OBJECT\.o : $DEPENDENCIES\n\tg++ -ansi -Wall -g -c $OBJECT\.cpp" > outtest

touch outtest

find . -name "*.cpp" | tr ' ' '\n' > file.txt

exec 3<&0
exec 0<file.txt
  while read line 
    do
	DEPENDENCIES=$(grep -i "#include" $line | sed s/"#include "//i | tr '\n' ' ')
	OBJECT=$(echo $line | sed s:./:: | sed s:.cpp::)
	echo -e "$OBJECT.o : $DEPENDENCIES\n\tg++ -ansi -Wall -g -c $2 $3 $4 $5 $OBJECT.cpp" >> outtest
        #echo $line | sed s/.\///
        #grep -i "#include" $line | sed s/#include//
    done
exec 0<&3

exit 1;
