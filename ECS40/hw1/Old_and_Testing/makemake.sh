#! /bin/bash
#Henry Walton, Lucian Novosel

if [ -z "$1" ]; then
	echo "usage: makemake.sh executable_name"
	exit 1
fi

#creates the Makefile
touch Makefile

#adds the first part of the first line, the executable name
echo -n "$1 : " > Makefile

#finds all the .cpp files in the current directory, and appends their names,
#rewritten with .o at the end, and appends them to Makefile
find . -name "*.cpp" | sed s:./:: | sed s:.cpp:'.o ': >> Makefile

#find -name "*.cpp" -printf "%P " | sed "s/\.cpp/\.o/g" >> Makefile

#removes the newlines so that the first line is properly in a list
cat Makefile | tr -d '\n' > Makefile

#writes the start of the second line
echo -e "\t g++ -ansi -Wall -g $2$3$4$5$6" > line2

find . -name "*.cpp" | sed s:./:: | sed s:.cpp:'.o ': >> line2

cat line2 | tr -d '\n' > line2

cat line2 | sed '$s/$/\n/' > line2

chmod 777 line2
chmod 777 Makefile

cat line2 >> Makefile

#find -name "*.cpp" -exec grep "#include" {} \; | sed s/#include// > Makefile
