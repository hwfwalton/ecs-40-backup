#! bin/bash/
#Henry Walton, Lucian Novosel

DEPENDENCIES=$(grep -i "#include" $1 | sed s/"#include "//i | tr '\n' ' ')

OBJECT=$(echo $1 | sed s/\.cpp/\.o/g)

echo -e "$OBJECT : $DEPENDENCIES\n\tg++ -ansi -Wall -g -c $1" > outtest

#find -name "*.cpp" -exec grep "#include" {} \; | sed s/#include//
