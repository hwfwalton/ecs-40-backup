//Henry Walton, Lucian Novosel
#ifndef DIRECTORY_H
   #define DIRECTORY_H
#include "permissions.h"

#define MAXSUBS 3

typedef struct Dir
{
	char *name;
	int Mod_time;
	int subCount;
	int umask;
	struct Dir* parent;
	struct Dir* subs[MAXSUBS];
	//Permissions();
}Directory;

void createDirectory(Directory *currDir, const char *name, int time, int umask, Directory *parent);

void makeDirectory(Directory *currDir, const char *name, int time, int umask);


#endif
