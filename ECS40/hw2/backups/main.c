// Author: Sean Davis
// Modified by: Henry Walton, Lucian Novosel
#include <stdlib.h>
#include "funix.h"

int main()
{
	Funix *funix = (Funix*) malloc(sizeof(Funix));

  run(funix);
  free (funix);
  return 0;
} // main()
