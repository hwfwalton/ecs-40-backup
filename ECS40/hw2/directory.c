//Henry Walton, Lucian Novosel
#include "directory.h"
#include "permissions.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

//void createDirectory(Directory *currDir, char *name, int time, int umask, Directory *parent);

void makeDirectory(Directory *currDir, const char *name, int time, int umask)
{
	int i;
	
	if(currDir->subCount>=MAXSUBS){
		printf("mkdir: %s already contains the maximum number of directories\n", currDir->name);
		return;
	}

	if (currDir->subCount<MAXSUBS){
		for (i=0;i<(currDir->subCount);i++){
			if (strcmp(currDir->sub1->name, name)==0){
				printf("Usage Error: Duplicate file\n");
				return;
			}
		}
	}

		Directory *newDir = (Directory *)malloc(sizeof(Directory));
		createDirectory(newDir, name, time, umask, currDir);
}




void createDirectory(Directory *currDir, const char *name, int time, int umask, Directory *parent)
{
	currDir->name=(char *)malloc(sizeof(strlen(name)));
	strcpy(currDir->name, name);	
	currDir->Mod_time = time; //assigns the int, increment should happen on mkdir 
	currDir->umask = umask; //assigns the umask. 
	currDir->parent=parent; 
	currDir->subCount=0;

	if (strcmp(currDir->name, "/") != 0){
		if (currDir->parent->subCount==2){
			currDir->parent->sub3=currDir;
		}

		else if (currDir->parent->subCount==1){
			currDir->parent->sub2=currDir;
		}	

		else{
			currDir->parent->sub1=currDir;
		}
		currDir->parent->subCount++;
	}
	//setPerms is permissions function 
	//setPermissions(currDir, umask); //Sets the permissions. 
}


