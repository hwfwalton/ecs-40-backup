//Henry Walton, Lucian Novosel
#include "directory.h"
#include "funix.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

// calls cd() with currentDirectory as one of its parameters
void cd(Funix *funix, int argCount, const char *argument[])
{

 if(strcmp(argument[1],funix->currentDirectory->sub1->name)==0)
                {printf("%s:no such directory",argument[1]);
                writePrompt(funix);}
           if(strcmp(argument[1],funix->currentDirectory->sub2->name)==0)
                {printf("%s:no such directory",argument[1]);
                writePrompt(funix);}
           if(strcmp(argument[1],funix->currentDirectory->sub3->name)==0)
                {printf("%s:no such directory",argument[1]);
                writePrompt(funix);}

}

// checks "exit" command, returns 0 on proper exit
int eXit(Funix *funix, int argCount, const char *arguments[])
{
	exit (1);
}

void getCommand(Funix *funix, char *command) // writes prompt and reads command
{
	fgets(command, 80, stdin);
	processCommand(funix, command);
}

// creates currentDirectory, and sets umask and time
void init(Funix *funix)
{
	funix->currentDirectory=(Directory*)malloc(sizeof(Directory));
	createDirectory(funix->currentDirectory,"/",0,0,NULL);
}

// calls ls() with currentDirectory as one of its parameters
void ls(Funix *funix, int argCount, const char *arguments[])
{
	if (funix->currentDirectory->subCount>0){
		printf("%s\n", funix->currentDirectory->sub1->name);
	}

	if (funix->currentDirectory->subCount>1){
		printf("%s\n", funix->currentDirectory->sub2->name);
	}

	if (funix->currentDirectory->subCount>2){
		printf("%s\n", funix->currentDirectory->sub3->name);
	}
}

// calls mkdir() with currentDirectory as one of its parameters
void mkdir(Funix *funix, int argCount, const char *arguments[])
{
	if(argCount > 2){
		printf("usage: mkdir directory_name\n");
		return;
	}

	makeDirectory(funix->currentDirectory, arguments[1], funix->time++, funix->umask);
}

int processCommand(Funix *funix, char *command) // returns 0 on proper exit
        {
        int argCount=0;
        const char* arguments[3];
        arguments[argCount]=strtok(command," ");
        
	for(argCount=1;argCount!=3;argCount++)
        {
                arguments[argCount]=strtok(NULL," \n"); 
        }
        
	if (arguments[2] != NULL){
		argCount=3;
	}
	else if (arguments[1] != NULL){
		argCount=2;
	}
	else{
		argCount=1;
	}

	if(strcmp(arguments[0],"mkdir")==0)
			mkdir(funix, argCount, arguments);
	else if(strcmp(arguments[0],"ls\n")==0)
			ls(funix,argCount, arguments);
	else if(strcmp(arguments[0],"cd")==0)
			cd(funix,argCount,arguments); 
	else if(strcmp(arguments[0],"umask")==0)
			umask(funix,argCount,arguments);
	else if(strcmp(arguments[0],"exit\n")==0)
			eXit(funix,argCount,arguments);	
	else{
	printf("%s: Command not found.", arguments[0]);
	}
	
	writePrompt(funix);	
	return 0;
}

void run(Funix *funix) // reads and processes commands until proper exit
{
	init(funix);
	writePrompt(funix);
}

// checks "umask" command and executes it if it is proper
void umask(Funix *funix, int argCount, const char *arguments[])
{
}

// shows path and '+'
void writePrompt(Funix *funix)
{
	showPath(funix->currentDirectory);
	printf(" # ");
	char input[80];
	getCommand(funix, input);
}

// prints the current path by recursing through the passed directory back to the root
void showPath(Directory *currDir)
{
	if (currDir->parent != NULL){
		showPath(currDir->parent);
	} 
	printf("%s", currDir->name);
}
