// Original Author: Sean Davis
// Edits: Henry Walton, Lucian Novosel

#ifndef DIRECTORY_H
	#define DIRECTORY_H
#include "Time.h"
#include "permissions.h"
#include <ctime>
#include <fstream>
#include <iostream>
#define MAX_DIRECTORIES 3
using namespace std;
class Directory
{
  char *name;
  Directory *subDirectories[MAX_DIRECTORIES];
  Time dirTime;
  int subDirectoryCount;
  Permissions permissions;
  Directory *parent;
public:
  Directory(const char *name, short umask, Directory *parent);
  ~Directory();
  Directory* cd(int argCount, const char *arguments[]);
  void ls(int argCount, const char *arguments[]) const;
  void mkdir(int argCount, const char *arguments[], short umask);
  void showPath() const;
  friend ostream& operator<<(ostream &outf, Directory &dir);
}; // class Directory
#endif

