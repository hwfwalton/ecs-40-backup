// Original Author: Sean Davis
// Edits: Henry Walton, Lucian Novosel

#include <iostream>
#include "permissions.h"
using namespace std;

void Permissions::print() const
{
  if(ownOctal & 4)
    cout << "r";
  else
    cout << "-";

  if(ownOctal & 2)
    cout << "w";
  else
    cout << "-";

  if(ownOctal & 1)
    cout << "x";
  else
    cout << "-";

  if(groOctal & 4)
    cout << "r";
  else
    cout << "-";

  if(groOctal & 2)
    cout << "w";
  else
    cout << "-";

  if(groOctal & 1)
    cout << "x";
  else
    cout << "-";

  if(worOctal & 4)
    cout << "r";
  else
    cout << "-";

  if(worOctal & 2)
    cout << "w";
  else
    cout << "-";

  if(worOctal & 1)
    cout << "x";
  else
    cout << "-";

} // print()

void Permissions::set(short originalPermissions,  short umask)
{
  ownOctal = originalPermissions & ~umask;
  groOctal = originalPermissions & ~umask;
  worOctal = originalPermissions & ~umask;
} // setPermissions()
