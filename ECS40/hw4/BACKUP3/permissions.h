// Original Author: Sean Davis
// Edits: Henry Walton, Lucian Novosel
#ifndef PERMISSIONS_H
	#define PERMISSIONS_H

class Permissions
{
  short ownOctal;
  short groOctal;
  short worOctal;
public:
  void print() const;
  void set(short originalPermissions, short umask);
};  // class Permissions

#endif
