// Original Author: Sean Davis
// Edits: Henry Walton, Lucian Novosel
#include "funix.h"
#include <fstream>
#include <stdlib.h>
#include <iostream>

int main()
{
  Funix *funix = new Funix();
  funix->run();
  delete funix;
} // main()
