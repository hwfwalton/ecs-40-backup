// Original Author: Sean Davis
// Edits: Henry Walton, Lucian Novosel
#ifndef FUNIX_H
  #define FUNIX_H

#include "directory.h"
#include "Time.h"
#include <fstream>

class Funix
{
  Directory *currentDirectory;
  int umask;
  Time dirTime;
  void cd( int argCount, const char *arguments[]);
  // calls cd() with currentDirectory as one of its parameters
  int eXit( int argCount, const char *arguments[]);
  // checks "exit" command, returns 0 on proper exit
  void getCommand( char *command); // writes prompt and reads command
   void ls( int argCount, const char *arguments[]);
  // calls ls() with currentDirectory as one of its parameters
  void mkdir( int argCount, const char *arguments[]);
  // calls mkdir() with currentDirectory as one of its parameters
  int processCommand( char *command); // returns 0 on proper exit
  void setUmask( int argCount, const char *arguments[]);
  // checks "umask" command and executes it if it is proper
  void writePrompt(); // shows path and '+'
  void chmod( int argCount, const char *arguments[]);
  void cp( int argCount, const char *arguments[]);
public:
  Funix(); // creates currentDirectory, and sets umask and time
  ~Funix();
  void run(); // reads and processes commands until proper exit
}; // Funix class
#endif

