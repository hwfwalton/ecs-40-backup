// Henry Walton, Lucian Novosel

#include <ctime>
#include <iostream>
#include <iomanip>
#include "Time.h"
using namespace std;

void Time::showDate() const
{ 
const char * months[] = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep",
                         "Oct","Nov","Dec"};

const tm * dateInfo = &timeInfo;

cout<<setfill('0')<<months[(dateInfo->tm_mon)]<<" "<<dateInfo->tm_mday<<" "<<setw(2)<<dateInfo->tm_hour
                                <<":"<<setw(2)<<dateInfo->tm_min<<":"<<setw(2)<<dateInfo->tm_sec;
} // main()

void Time::setDate()
{
time_t timSecs;
time(&timSecs);
timeInfo = *(localtime(&timSecs));
}
/*
ostream& operator<<(ostream &outf, Time &tim)
{
  struct tm * dateInfo = &timeInfo;
  return outf<<timeInfo.tm_mon<<" "
	  <<timeInfo.tm_mday<<" "
	  <<timeInfo.tm_hour<<" "
	  <<timeInfo.tm_min<<" "
	  <<timeInfo.tm_sec;
}
*/
