// Henry Walton, Lucian Novosel
#ifndef TIME_H
        #define	TIME_H
#include <ctime>
class Time
{
  struct tm timeInfo;
public:
  void showDate() const;
  void setDate();
};

#endif
