// Author: Sean Davis

#include <ctime>
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
  time_t timSecs;
  struct tm * timeInfo;
  
  const char * months[] = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

  time(&timSecs);

  timeInfo = localtime (&timSecs);
  cout<<months[(timeInfo->tm_mon)]<<" "<<timeInfo->tm_mday<<" "<<timeInfo->tm_hour<<":"<<timeInfo->tm_min<<":"<<timeInfo->tm_sec<<endl;
 
  return 0;
} // main()
