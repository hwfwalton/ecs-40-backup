// Original Author: Sean Davis
// Edits: Henry Walton, Lucian Novosel

#include <iostream>
#include "permissions.h"
using namespace std;

void Permissions::print() const
{
for(int i=0;  i <3; i++)
  if(octal[i] & 4)
    cout << "r";
  else
    cout << "-";

  if(octal[i] & 2)
    cout << "w";
  else
    cout << "-";

  if(octal[i] & 1)
    cout << "x";
  else
    cout << "-";
} // print()

void Permissions::set(short originalPermissions,  short umask)
{
  octal = originalPermissions & ~umask;
} // setPermissions()
